package hr.ferit.brunozoric.jsondemo

import com.google.gson.annotations.SerializedName

data class Character (
    @SerializedName("name") val name: String,
    @SerializedName("level") val level: Int,
    @Transient val isAlive: Boolean
)