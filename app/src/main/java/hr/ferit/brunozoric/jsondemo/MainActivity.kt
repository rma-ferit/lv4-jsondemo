package hr.ferit.brunozoric.jsondemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val gson: Gson = GsonBuilder().create()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpUi()
    }

    private fun setUpUi() {
        convertToObjectAction.setOnClickListener{ convertFromJsonAndDisplay() }
        convertToJsonAction.setOnClickListener{ convertToJsonAndDisplay() }
    }

    private fun convertToJsonAndDisplay() {
        val name = characterNameInput.text.toString()
        val level = characteLevelInput.text.toString().toInt()

        val character = Character(name, level, isAlive = true)
        val jsonCharacter = gson.toJson(character)

        jsonDisplay.text = jsonCharacter
    }

    private fun convertFromJsonAndDisplay(){
        val jsonCharacter = jsonDisplay.text.toString()
        val character = gson.fromJson(jsonCharacter, Character::class.java)

        objectDisplay.text = character.toString()
    }
}
